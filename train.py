import os
import train_functions
import sys
import json
import argparse


# Функция отвечает за обработку входных аргументов
def process_arguments(input_list):
    parser = argparse.ArgumentParser()
    parser.add_argument('--input-dir', dest='dir',
                        type=str, help=' путь к директории,'
                                       ' в которой лежит коллекция документов. '
                                       'Если данный аргумент не задан, считать, '
                                       'что тексты вводятся из stdin')
    parser.add_argument('--model', dest='model', required=True,
                        type=str, help='путь к файлу,'
                                       ' в который сохраняется модель')
    parser.add_argument('--lc', dest='lowercase',
                        action='store_true', help='необязательный аргумент.'
                                                  ' Приводить ли тексты к lowercase')
    return parser.parse_args(input_list)


# Основной метод работы программы
def solve():
    args = process_arguments(input().split())
    model = dict()
    file_names = list()
    cnt = 1
    if args.dir is not None:
        file_names = os.listdir(args.dir)
        cnt = len(file_names)
    for i in range(cnt):
        try:
            input_reader = sys.stdin if len(file_names) == 0 else open(args.dir + '/' + file_names[i], 'r')
        except EOFError:
            continue
        for inputString in input_reader:
            model = train_functions.process_text(
                train_functions.parse_string(inputString if args.lowercase else inputString.lower()),
                model
            )
        if input_reader != sys.stdin:
            input_reader.close()
    model_file = open(args.model, 'w')
    model_file.write(json.dumps(model))
    model_file.close()


if __name__ == '__main__':
    solve()
