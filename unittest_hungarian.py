import unittest
from scipy import optimize
import numpy as np

class TestHungarianAlgorithm(unittest.TestCase):

    def test(self):
        row_ind, col_ind = optimize.linear_sum_assignment(
            [
                [0, 1, 1],
                [1, 1, 0],
                [1, 0, 1]
            ]
        )
        self.assertEqual(list(row_ind), [0, 1, 2])
        self.assertEqual(list(col_ind), [0, 2, 1])


if __name__ == '__main__':
    unittest.main()