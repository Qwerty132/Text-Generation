import unittest
import train_functions
import train


class TestTrainFunctions(unittest.TestCase):

    def test_upper(self):
        test_string = train_functions.parse_string('A11C')
        self.assertEqual(test_string, 'A  C')
        test_string = train_functions.parse_string('AAB 1  QWERTY')
        self.assertEqual(test_string, 'AAB    QWERTY')

    def test_down(self):
        test_string = train_functions.parse_string('a 76543 node')
        self.assertEqual(test_string, 'a       node')
        test_string = train_functions.parse_string(' a1a1a1a1aa111')
        self.assertEqual(test_string, ' a a a a aa   ')


class TestParsingArguments(unittest.TestCase):

    def test_simple(self):
        arg = train.process_arguments(['--model', '123'])
        self.assertEqual(arg.model, '123')

    def test_exception(self):
        self.assertRaises(
            BaseException,
            train.process_arguments,
            # args
            ['--lc']
        )

    def test_lc(self):
        arg = train.process_arguments(['--model', 'output.txt', '--lc'])
        self.assertTrue(arg.lowercase)


if __name__ == '__main__':
    unittest.main()
