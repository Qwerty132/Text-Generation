# Функция, приводящая строку к нормальному виду
def parse_string(string):
    all_remove_digits = set()
    for digit in string:
        if not digit.isalpha():
            all_remove_digits.add(digit)
    for digit in all_remove_digits:
        string = string.replace(digit, ' ')
    return string


# Функция обработки строчки текста и добавления ее в модель
def process_text(text_, model_):
    for last_word_, word_ in zip(text_.split(), text_.split()[1:]):
        model_.setdefault(word_, dict())
        model_[word_].setdefault(last_word_, 0)
        model_[word_][last_word_] += 1
    return model_
