import unittest
import generate_functions
import random
import generate
from string import ascii_uppercase
from string import ascii_lowercase


def generate_word(len_):
    result = ''
    for iterate in range(len_):
        if random.randint(0, 1) == 0:
            result += random.choice(ascii_uppercase)
        else:
            result += random.choice(ascii_lowercase)
    return result


def generate_model(size_, len_):
    model = dict()
    for iterate in range(size_):
        word = generate_word(len_)
        model.setdefault(word, dict())
        for s_iterate in range(size_):
            next_word = generate_word(len_)
            model[word].setdefault(next_word, 0)
            model.setdefault(next_word, dict())
            model[word][next_word] += 1
    return model


class TestGeneratorWordsModel(unittest.TestCase):

    def test_simple(self):
        model = generate_model(20, 5)
        self.assertTrue(generate_functions.get_random_word(model) in model)

    def TestChooseWord(self):
        model = generate_model(30, 10)
        word = generate_functions.get_random_word(model)
        for iterate in range(10000):
            word = generate_functions.get_next_word(model, word)
            self.assertTrue(word in model)


class TestParsingArguments(unittest.TestCase):

    def test_simple(self):
        arg = generate.process_arguments(['--model', '123', '--length', '20'])
        self.assertEqual(arg.model, '123')


class TestGenerationSequence(unittest.TestCase):

    def test_sequence(self):
        model = generate_model(70, 9)
        result_list = generate_functions.generate_sequence(
            model,
            generate_functions.get_random_word(model),
            100
        )
        prev_word = None
        for word in result_list:
            self.assertTrue(word in model)
            if prev_word is None:
                continue
            self.assertTrue(len(model[prev_word]) == 0 or
                            (len(model[prev_word]) != 0 and word in model[prev_word]) )


if __name__ == '__main__':
    unittest.main()
