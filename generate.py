import sys
import json
import generate_functions
import argparse


# Функция отвечает за обработку входных аргументов
def process_arguments(input_list_):
    parser = argparse.ArgumentParser()
    parser.add_argument('--seed', dest='start_word', type=str,
                        help='Начальное слово. Если не указано,'
                        ' выбираем слово случайно из всех слов')
    parser.add_argument('--model', dest='model', required=True,
                        type=str, help='путь к файлу,'
                        ' из которого сохраняется модель')
    parser.add_argument('--length', dest='len', type=int, required=True,
                        help='длина генерируемой последовательности')
    parser.add_argument('--output', dest='output', type=str,
                        help='Файл, в который будет записан результат. '
                        'Если аргумент отсутствует, выводить в stdout')
    return parser.parse_args(input_list_)


# Основной метод программы
def solve():
    args = generate_functions.process_arguments(input().split())
    model_file = open(args.model, "r")
    model = json.loads(model_file.read())
    model_file.close()
    output_list = generate_functions.generate_sequence(
        model,
        generate_functions.get_random_word(model) if args.start_word is None
        else args.start_word,
        args.len
    )
    output_writer = sys.stdout if args.output is None else open(args.output)
    output_writer.write(' '.join(output_list))
    if output_writer != sys.stdout:
        output_writer.close()


if __name__ == '__main__':
    solve()
