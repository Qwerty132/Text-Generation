import random


# Функция возвращает случайное слово среди модели
def get_random_word(model_):
    if len(model_) == 0:
        raise Exception("Empty model")
    return random.choice(list(model_.keys()))


# Функция возвращает следующее слово, идущее на word, с учетом частоты
def get_next_word(model_, word_):
    if word_ not in model_:
        raise Exception("There is no {0} in model".format(word_))
    if len(model_[word_]) == 0:
        return get_random_word(model_)
    summ = 0
    for next_word in model_[word_]:
        summ += model_[word_][next_word]
    summ -= random.randint(0, summ)
    for next_word in model_[word_]:
        summ -= model_[word_][next_word]
        if summ <= 0:
            return next_word
    raise Exception("Incorrect work of method 'get_next_word'")


# Функция генерации последовательности по первому слову
def generate_sequence(model_, first_word_, size_sequence_):
    result = list()
    result.append(first_word_)
    for iterate in range(1, size_sequence_):
        result.append(
            get_next_word(model_, result[-1])
        )

    return result
